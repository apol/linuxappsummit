---
layout: page
title: "Linux App Summit"
permalink: /
---

# LAS 2019  
The Linux App Summit (LAS) is designed to accelerate the growth of the Linux application ecosystem 
by bringing together everyone involved in creating a great Linux application user experience. 

At LAS you can attend talks, panels, and Q&As on a wide range of topics covering everything from creating, 
packaging, and distributing apps, to monetization within the Linux ecosystem, designing beautiful applications, and more 
-- all delivered by the top experts in each field. You will acquire insights into how to reach users, 
build a community around your app, what toolkits and technologies make development easier, which platforms to aim for, and much more.

LAS welcomes application 
developers, designers, product managers, user experience specialists, community leaders, academics, and anyone who is 
interested in the state of Linux application design and development!

Stay tuned for more information!
